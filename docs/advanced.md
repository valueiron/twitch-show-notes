# Advanced

This section assumes you have worked in IT before or consider yourself a more of a *power user* or are just curious about experimenting and figuring out how things work. 
The goal of this is to dive deep and learn new IT realated skills with a focus System Administration, Networking, and Security

## Topics

- [ ] Markdown
- [ ] Vim
- [ ] Linux/Bash
- [ ] Python
- [ ] Windows Server/Active Directory
- [ ] Network Security Monitoring, Zeek, Rita, Elk Stack, Security Onion, HELK

 
## Keyboard Shortcuts

## Vim

|Key Combo|Description|
|---------|-----------|
|++ctrl+"x"+ctrl+"f"++|filename completion|
|++ctrl+"n"++|simple word completion|

`z=` Spell check when on highlighted word mispelled
