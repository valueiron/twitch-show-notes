# Live Chat Commands

|Command|Description|
|---------|-----------|
|!uptime|See how long the stream has been live for.|
|!8ball [Question]|Ask our magic 8 ball a question and get an answer |
