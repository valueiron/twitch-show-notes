# Intermediate

This section if for people that are familar with computer basics and feel comfortable navigating around windows 10.
The goal of this section is to help people that want to become more productive and effecient on your PC. 


## Topics


- [ ] Productivity Tips and Tricks
- [ ] IT Terminology `to better communicate with IT even if you're in a different career field`
- [ ] Understand more about your web browser
- [ ] Keyboard Shortcuts
- [ ] File Types
- [ ] Google-fu
- [ ] Troubleshooting Methodology `Going over material related to IT certifications like the Comptia IT Fundamentals/A+.`

## Google Foo

1. Every word matters
2. Order matters
3. Captilization doesn't matter
4. Punctuation doesn't matter
5. There can be some exceptioins to these

---
1. "" - Search surrounded by quotes for exact phrase
2. * - Acts as a wildcard and will match any word or phrase
3. define: - Built in dictionary
4. filetype: - Restrict search to cetain filetypes ex: pdf,docx,txt,ppt
5. site: - Limit seach to a specific website
6. related: - Find websites related to a domain ex:apple.com
7. intitle: - Find sites with specific words in the title. 
8. inurl: - Search for words in a specified url
9. intext: - Search for words in the content of a page
10. weather: - Automatic weather ex:weather:las vegas
11. map: - Show map results for a location
12. in - The in keyword can be used for easy conversions $100 in GBP
13. source: - Get news results from a specificed source apple source:cnn
14. number..number - Will narrow results between years ex: wwdc video 2010..2014
15. -word - Exclude word from search to narrow results

## Keyboard Shortcuts

|Key Combo|Description|
|---------|-----------|
|++win+shift+esc++|Opens task manager|
|++alt+f4++|Exit current program|
